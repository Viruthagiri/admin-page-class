=== Wordpress Admin Page Class ===
Contributors: bainternet 
Donate link:http://en.bainternet.info/donations
Tags: admin page, option page, options panel, admin options panel
Requires at least: 2.9.2
Tested up to: 3.3.1
Stable tag: 0.1

The Admin Page Class is used by including it in your plugin files and using its methods to create custom Admin Pages. It is meant to be very simple and straightforward. 
 

== Description ==

The Admin Page Class is used by including it in your plugin files and using its methods to create custom Admin Pages. It is meant to be very simple and straightforward. 
for usage Take a look at the `class-usage-demo.php` file which can also be tested as a WordPress Plugin. Other options are available for each field which can be see in the 'admin-page-class.php' file,


== Installation ==
Simple steps:  

1.  Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation.
2.  Then activate the Plugin from Plugins page.
3.  Done!

== Frequently Asked Questions ==
=What are the requirements?=

PHP 5.2 and up.


== Changelog ==
= 0.1 = 
Initial public release